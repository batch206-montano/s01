import java.text.DecimalFormat;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scannerName = new Scanner(System.in);

        System.out.println("First Name:");
        String firstName = scannerName.nextLine();

        System.out.println("Last Name:");
        String lastName = scannerName.nextLine();

        System.out.println("First Subject Grade:");
        double subjGrade1 = scannerName.nextDouble();

        System.out.println("Second Subject Grade:");
        double subjGrade2 = scannerName.nextDouble();

        System.out.println("Third Subject Grade:");
        double subjGrade3 = scannerName.nextDouble();

        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);

        double avgGrade = (subjGrade1 + subjGrade2 + subjGrade3) / 3;

        System.out.println("Good day, " +firstName +" " +lastName +". " +"Your grade average is: " +df.format(avgGrade));

    }
}